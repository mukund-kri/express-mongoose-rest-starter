'use strict';

var expect       = require('chai').expect;
var supertest    = require('supertest');

var app          = require('../../../server/app');


describe('Home page', function() {

  it('#get request should be succesful', function(done) {
    supertest(app)
      .get('/')
      .expect('content-type', /text/)
      .expect(200)
      .end(function(err, res) {
        if(err) throw err;

        expect(res.text).to.match(/Your\ App/);
        done();
      });

  })
})
