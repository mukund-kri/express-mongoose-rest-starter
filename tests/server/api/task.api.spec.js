'use strict';

var expect       = require('chai').expect;
var mongoose     = require('mongoose');
var supertest    = require('supertest');
var fixtures     = require('node-mongoose-fixtures');

var app          = require('../../../server/app');
var config       = require('../../../server/config/server.config');
var Task         = require('../../../server/models/task');

var clearDB      = require('mocha-mongoose')(config.mongoConfig.test.url, {noClear: true});

describe('Task API', function() {

  before(function (done) {
    if (mongoose.connection.db) return done();
    mongoose.connect(config.mongoConfig.test.url, done);
  });

  before( function (done) {
    fixtures(
      require('../fixtures/many.tasks'),
      function(err, docs) {
        done();
      }
    );
  });

  after(function (done) {
    clearDB(done);
  });

  it('#GET should return all tasks', function(done) {
    supertest(app)
      .get('/api/task')
      .expect('content-type', /json/)
      .expect(200)
      .end(function(err, res) {
        if(err) throw err;

        expect(res.body.length).to.equal(2);
        expect(res).deep.property('body.[0].text', 'Task 1');
        done();
      })
  });

  it('#POST should save a task', function (done) {
   supertest(app)
     .post('/api/task')
     .send({ text: 'posted task' })
     .expect(200)
     .expect('content-type', /json/)
     .end(function(error, res) {
       if(error) throw error;

       expect(res.body.message).to.equal('success');
       expect(res.body.task._id).to.be.ok;
       expect(res.body).deep.property('task.text', 'posted task');

       // Check the DB if a record is created
       Task.find({'text': 'posted task'})
         .then(function(doc) {
           expect(doc[0]).to.be.ok;
           expect(doc[0]._id).to.be.ok;
         })
         .then(done)
     });
  });

  it("#DELETE Should remove record", function(done) {
    supertest(app)
      .delete('/api/task/500000000000000000000001')
      .expect(200)
      .expect('content-type', /json/)
      .end(function(error, res) {
        if(error) throw error;
        expect(res.body.message).to.equal('success');

        // check if document deleted from the db
        Task.findById('500000000000000000000001')
        .then(function(doc) {
          expect(doc).to.not.be.ok;
        })
        .then(done);
      })
  });

  it("#GET with id should return a single task", function(done) {
    supertest(app)
      .get('/api/task/500000000000000000000002')
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if(err) throw err;

        var task = res.body;
        expect(task).to.be.ok;
        expect(task).property('text', 'Task 2');

        done();
      });
  });

  it('#PUT should update document', function(done) {
    supertest(app)
      .put('/api/task/500000000000000000000002')
      .send({ text: 'updated task'})
      .expect(200)
      .expect('content-type', /json/)
      .end(function(err, res) {
        if(err) throw err;

        expect(res.body).property('message', 'success');

        Task.findById('500000000000000000000002')
          .then(function(doc) {
            expect(doc).property('text', 'updated task');
          })
          .then(done);
      })
  });

});
