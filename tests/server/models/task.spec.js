'use strict';

var expect   = require('chai').expect;
var Task     = require('../../../server/models/task.js');

var dbURI    = require('../../../server/config/server.config').mongoConfig.test.url;
var clearDB  = require('mocha-mongoose')(dbURI, {noClear: true});
var mongoose = require('mongoose');

var fakerHelper = require('./faker.helper');



describe("Task Model", function() {

  // Before any test is run, make sure the connection with mongo is up
  before(function(done) {
    if (mongoose.connection.db) return done();
    mongoose.connect(dbURI, done);
  });

  // Clear the database all data before running the tests
  after(function(done) {
    clearDB(done);
  });

  it("#Can be instantiate and saved", function(done) {
    var task = new Task({text: 'Task 1'});
    task.save()
      .then(function(doc) {
        // The side effect of document creation. An version __v is assigned to the doc
        expect(doc.__v).to.be.not.null;
        expect(doc).property('status', false);
      })
      .done(done);
  });

  it("#Can be created", function(done) {
    // Use faker to generate data.
    var fakeTaskData = fakerHelper.genTaskData();

    Task.create(fakeTaskData)
    .then(function(doc) {
      expect(doc.__v).not.to.be.null;
    })
    .done(done);
  });

  it('#Can be listed', function(done) {
    Task.find({})
    .then(function(docs) {
      expect(docs.length).to.equal(2);
    })
    .done(done);
  });

  // TODO: You know the drill, add your tests here ...
})
