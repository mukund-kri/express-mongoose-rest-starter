var faker = require('faker');

module.exports = {
  genTaskData: function() {
    return {
      text: faker.lorem.sentence(6)
    }
  }
}
