# By default debian jessie does not have curl. so install.
apt-get install -y curl

# Install node version 6.x
curl -sL https://deb.nodesource.com/setup_6.x | bash -
apt-get install -y nodejs

# Install mongodb
sudo apt-get install -y mongodb

# Some more essentials for development
apt-get install -y build-essential

# Install required global npm packages
npm install -g gulp
