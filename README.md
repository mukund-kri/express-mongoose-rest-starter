# Express+Mongoose REST API project starter #

Provides a starter project for developing REST api with express and mongoose.

## dependencies ##

 1. Vagrant.

 I'm using vagrant 1.8.x and virtual box 5.0.x. This project should work on most
 recent versions on vagrant.

 1. node.js.

 The current vagrant uses node 6.x but since this project is coded in pure es5
 any node 10.x and above should work fine.

 1. Libs

 Express 4, mongoose, mocha, chai ..., read the packge.json for a full list.

## How to use ##

 1. clone the repo.

 2. Start vagrant.

 3. Install npm dependencies.

 4. Start coding ...

## Project Structure ##
