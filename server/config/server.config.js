'use strict';

var path  = require('path');


var expressConfig = {
    'views'       : path.resolve(__dirname, '../', 'views'),
    'view engine' : 'pug'
};

var mongoConfig = {
  dev: {
       url: 'mongodb://localhost/ppdev'
   },

   test: {
       url: 'mongodb://localhost/pptest'
   }
};

// Make bluebird the default Promise provider
var mongoose     = require('mongoose');
mongoose.Promise = require('bluebird');

module.exports = {
  expressConfig: expressConfig,
  mongoConfig: mongoConfig
}
