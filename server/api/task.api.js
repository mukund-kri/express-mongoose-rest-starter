'use strict';

var express    = require('express');

var Task       = require('../models/task');


var router = express.Router();

// The tasks API

router.get('/', function(req, res) {
  Task.find({})
  .then(function(data) {
    res.json(data);
  })
  // any other error is a 500
  .catch(function(err) {
    res.status(500).json({ error: 'Internal Error'});
  })
});

router.post('/', function (req, res) {
  var task = new Task({
    text: req.body.text
  });

  task.save()
    .then(function(doc) {
      res.json({'message': 'success', 'task': doc})
    })
    .catch(function(err) {
      res.status(500).json(err);
    });
});

router.delete('/:id', function(req, res) {
  var docId = req.params.id;

  Task.remove({_id: docId})
    .then(function(doc) {
      res.status(200).json({message: 'success'});
    })
    .catch(function(error) {
      res.status(500).json({error: 'error deleteing document'});
    });
});

router.get('/:id', function(req, res) {
  var docId = req.params.id;

  Task.findById(docId)
    .then(function(doc) {
      if(doc) res.json(doc);
      else res.status(404).json({message: 'task not found'});
    })
    .catch(function(error) {
      res.status(500).json({message: 'internal error'});
    })
});

router.put('/:id', function(req, res) {
  var docId = req.params.id;

  var update = {$set: {}};
  for(var key in req.body) {
    update.$set[key] = req.body[key]
  };

  Task.findByIdAndUpdate(
    docId,
    update
  )
  .then(function(doc) {
    res.json({ message: 'success' });
  })
  .catch(function(err) {
    res.status(500).json(err);
  })
});

module.exports = router;
