'use strict';

var path          = require('path');
var express       = require('express');
var bodyParser    = require('body-parser');

var config        = require('./config/server.config');
var jsonHelper    = require('./helpers/json');


// Instantiate a express instances
var app = express();

// Configure express
for(var prop in config.expressConfig) {
  app.set(prop, config.expressConfig[prop]);
}

// Modify the json returned by express
app.set('json replacer', jsonHelper.replacer);

// Register third party middleware

// for parsing application/json
app.use(bodyParser.json());
// for parsing application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


// Register other routes
var homeRoutes = require('./routes/home');
app.use('/', homeRoutes);

// Register API routes
var taskApiRoutes = require('./api/task.api');
app.use('/api/task', taskApiRoutes);


module.exports = app;
