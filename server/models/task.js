var mongoose    = require('mongoose');


var taskSchema = mongoose.Schema({
  text:       { type: String, required: true },
  createTime: { type: Date, default: Date.now },
  status:     { type: Boolean, default: false }
});

module.exports = mongoose.model('Task', taskSchema);
