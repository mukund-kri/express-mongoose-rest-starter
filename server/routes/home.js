'use strict';

var express = require('express');


// Create a router for main pages
const router = express.Router();

// Home page
router.get('/', function(req, res) {
  res.render('index');
});

module.exports = router;
