'use strict';

var gulp     = require('gulp');
var server   = require('gulp-develop-server');
var mocha    = require('gulp-spawn-mocha');
var runSeq   = require('run-sequence');


var SERVER_CODE = './server/**/*.js';

/* -------------------------- DEV TASKS ------------------------------------- */
// Start the express server with gulp-develop-server
gulp.task('server:start', function() {
    server.listen({ path: './server/develop.js'});
});

// When ever there is a change in the server code, restart the express server
gulp.task('watch:server', function() {
  gulp.watch(SERVER_CODE, server.restart);
});

// Aggrigate all development tasks
gulp.task('develop', ['server:start', 'watch:server']);

/* --------------------------- TESTING TASKS -------------------------------- */
gulp.task('test:models', function(done) {
  return gulp.src(['tests/server/models/*.spec.js'])
    .pipe(mocha());

    done();
})

gulp.task('test:routes', function(done) {
  return gulp.src(['tests/server/routes/*.spec.js'])
    .pipe(mocha());

    done();
});

// Test the api routes
gulp.task('test:api', function(done) {
  return gulp.src(['tests/server/api/*.spec.js'])
    .pipe(mocha());

    done();
});

// All tests since they are DB related run in sequence
gulp.task('test', function(done) {
  runSeq('test:models', 'test:routes', 'test:api');
});

// The default task is just an alias for the develop task
gulp.task('default', ['develop']);
